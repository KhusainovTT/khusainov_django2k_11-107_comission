from django.shortcuts import render
from django.views.generic import TemplateView

class About(TemplateView):
    template_name = 'website/index.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['name'] = 'text'
        context['category'] = 'text'
        context['review_text'] = """
        text
        """
        return context