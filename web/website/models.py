from django.db import models


class Field(models.Model):
    name = models.CharField(max_length=20)
    category = models.CharField(max_length=20)
    review_text = models.CharField(max_length=1000)
