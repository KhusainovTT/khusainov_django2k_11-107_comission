from django import forms

class BigForm(forms.Form):
    name = forms.CharField(max_length=20)
    category = forms.CharField(max_length=20)
    review_text = forms.CharField(max_length=1000)